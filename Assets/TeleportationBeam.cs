﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeleportationBeam : MonoBehaviour
{

    public Valve.VR.EVRButtonId buttonId = Valve.VR.EVRButtonId.k_EButton_Axis0;

    public Transform reticle;
    public LineRenderer laser;
    public float range;
    public Transform player;

    public Color enabledColor;
    public Color disabledColor;


    private Light reticleLight;

    private SteamVR_TrackedObject controller;

    private RaycastHit target;
    private bool canTeleport;

    // Use this for initialization
    void Start()
    {

        reticleLight = reticle.gameObject.GetComponent<Light>();
        controller = GetComponent<SteamVR_TrackedObject>();
    }

    // Update is called once per frame
    void Update()
    {

        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);

        if (device.GetPress(buttonId))
        {

            canTeleport = false;

            laser.gameObject.SetActive(true);
            reticle.gameObject.SetActive(true);

            RaycastHit hit;
            Ray ray = new Ray(transform.position, transform.forward);

            List<Vector3> waypoints = new List<Vector3>();
            waypoints.Add(transform.position);

            reticle.position = ray.origin + ray.direction * range;

            reticleLight.color = disabledColor;
            laser.SetColors(disabledColor, disabledColor);

            if (Physics.Raycast(ray, out hit, range))
            {

                reticle.position = hit.point;

                reticleLight.color = enabledColor;
                laser.SetColors(enabledColor, enabledColor);

                target = hit;
                canTeleport = true;
            }

            waypoints.Add(reticle.position);

            laser.SetVertexCount(waypoints.Count);
            laser.SetPositions(waypoints.ToArray());

        }
        else
        {

            laser.gameObject.SetActive(false);
            reticle.gameObject.SetActive(false);

        }

        if (device.GetPressUp(buttonId) && canTeleport)
        {

            player.position = target.point;
            player.up = target.normal;
        }
    }
}